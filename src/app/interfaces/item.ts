export interface Item {
  id: number;
  name: string;
  url?: string;
  imageurl?: string;
  price: number;
  category: number;
  disabled: boolean;
}

export interface AddItem {
  id?: number;
  name: string;
  url?: string;
  price: number;
  category: number;
  imageurl?: string;
  disabled?: boolean;
}
