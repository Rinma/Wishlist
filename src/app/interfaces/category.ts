export interface Category {
  id: number;
  inserted_at?: string;
  updated_at?: string;
  name: string;
  key: string;
}

export interface AddCategory {
  id?: number;
  inserted_at?: string;
  updated_at?: string;
  name: string;
  key: string;
}
