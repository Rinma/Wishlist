import {Injectable} from '@angular/core';
import {AuthChangeEvent, createClient, Session, SupabaseClient} from "@supabase/supabase-js";
import {environment} from "../../environments/environment";
import {AddItem} from "../interfaces/item";
import {AddCategory} from "../interfaces/category";

@Injectable({
  providedIn: 'root'
})
export class SupabaseService {
  private supabase: SupabaseClient;

  constructor() {
    this.supabase = createClient(environment.supabaseUrl, environment.supabaseKey);
  }

  get user() {
    return this.supabase.auth.user();
  }

  get session() {
    return this.supabase.auth.session();
  }

  get profile() {
    return this.supabase
      .from('profiles')
      .select(`username, website, avatar_url`)
      .eq('id', this.user?.id)
      .single();
  }

  authChanges(callback: (event: AuthChangeEvent, session: Session | null) => void) {
    return this.supabase.auth.onAuthStateChange(callback);
  }

  signIn(email: string) {
    return this.supabase.auth.signIn({email});
  }

  signOut() {
    return this.supabase.auth.signOut();
  }

  writeItem(item: AddItem) {
    return this.supabase.from('items').upsert(item, {
      returning: "minimal"
    });
  }

  fetchItems() {
    return this.supabase
      .from('items')
      .select(`*`)
      .order(`id`);
  }

  readItem(id: number) {
    return this.supabase
      .from('items')
      .select(`*`)
      .eq('id', id);
  }

  async deleteItem(id: number) {
    await this.supabase
      .from('items')
      .delete()
      .match({id});
  }

  fetchCategories() {
    return this.supabase
      .from('categories')
      .select(`*`);
  }

  readCategory(id: number) {
    return this.supabase
      .from('categories')
      .select(`*`)
      .eq('id', id);
  }

  async writeCategory(category: AddCategory) {
    const {data, error} = await this.supabase.from('categories').upsert(category, {
      returning: "minimal"
    });

    if (error) {
      console.error(error);
    }

    return data;
  }

  async deleteCategory(id: number) {
    await this.supabase.from('categories').delete().match({id})
  }
}
