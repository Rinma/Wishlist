import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ItemComponent} from './item.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ItemComponent],
      imports: [HttpClientTestingModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.item = {
      id: 1,
      name: "Test Item",
      url: "",
      imageurl: "",
      category: 1,
      price: 12.99,
      disabled: false,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
