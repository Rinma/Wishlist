import {Component, Input} from '@angular/core';
import {Item} from "../interfaces/item";
import {Category} from "../interfaces/category";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent {

  @Input() category!: Category;
  @Input() items!: Item[];

  constructor() {
  }

  showCategory(): boolean {
    return this.items.length > 0;
  }
}
