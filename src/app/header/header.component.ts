import {Component, Input} from '@angular/core';
import {Category} from "../interfaces/category";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() categories: Category[] = [];

  constructor() {
  }
}
