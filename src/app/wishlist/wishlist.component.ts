import {Component, OnInit} from '@angular/core';
import {SupabaseService} from "../services/supabase.service";
import {Category} from "../interfaces/category";
import {Item} from "../interfaces/item";

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

  categories: Category[] = [];
  items: Item[] = [];

  constructor(private supabase: SupabaseService) { }

  ngOnInit(): void {
    this.fetchCategories().then(() => {
      console.log(this.categories)
    });

    this.fetchItems().then(() => {
      console.log(this.items)
    });
  }

  async fetchItems(): Promise<void> {
    let {data, error} = await this.supabase.fetchItems();

    if (error) {
      console.error(error);
      return;
    }

    this.items = data ?? [];
  }

  async fetchCategories(): Promise<void> {
    let {data, error} = await this.supabase.fetchCategories();

    if (error) {
      console.error(error);
      return;
    }

    this.categories = data ?? [];
  }

  getItemsForCategory(id: number): Item[] {
    return this.items.filter(item => {
      return item.category === id;
    }).sort(this.alphabetSort);
  }

  getNonEmptyCategories(): Category[] {
    return this.categories.filter(category => {
      return this.items.some(item => {
        return category.id === item.category;
      });
    }).sort(this.alphabetSort);
  }

  alphabetSort(first: Item | Category, second: Item | Category) {
    if (first.name.toUpperCase() < second.name.toUpperCase()) {
      return -1;
    }

    if (first.name.toUpperCase() > second.name.toUpperCase()) {
      return 1;
    }

    return 0;
  }
}
