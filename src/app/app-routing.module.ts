import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./admin/login/login.component";
import {WishlistComponent} from "./wishlist/wishlist.component";
import {AdminComponent} from "./admin/admin.component";
import {SessionGuard} from "./session.guard";

const routes: Routes = [
  {
    path: 'admin/login',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [SessionGuard]
  },
  {
    path: '**',
    component: WishlistComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
