import {Component} from '@angular/core';
import {SupabaseService} from "../../services/supabase.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  email: string = '';
  loading: boolean = false;

  constructor(private supabase: SupabaseService) {
  }

  async login() {
    try {
      this.loading = true;
      await this.supabase.signIn(this.email)
    } catch (error) {
      console.error(error);
    } finally {
      this.loading = false;
    }
  }

}
