import {Component, OnInit} from '@angular/core';
import {MenuItem} from "primeng/api";
import {SupabaseService} from "../services/supabase.service";
import {AddItem, Item} from "../interfaces/item";
import {AddCategory, Category} from "../interfaces/category";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  DIALOG_ACTION_CREATE = "create";
  DIALOG_ACTION_UPDATE = "update";

  menuItems!: MenuItem[];
  tabs!: MenuItem[];
  activeTab!: MenuItem;
  categories!: Category[];
  items!: Item[];

  showNewCategoryDialog: boolean = false;
  newCategoryAction: string = this.DIALOG_ACTION_CREATE;

  newCategoryName: string = "";
  newCategoryKey: string = "";
  newCategoryId: number = 0;

  newItemId: number | undefined = 0;
  newItemName: string = "";
  newItemUrl: string | undefined = "";
  newItemImageurl: string | undefined = "";
  newItemPrice: number = 0;
  newItemCategory: number = 0;
  newItemDisabled: boolean | undefined = false;
  newItemAction: string = this.DIALOG_ACTION_CREATE;

  showNewItemDialog: boolean = false;

  constructor(private supabase: SupabaseService) {
  }

  ngOnInit(): void {
    this.menuItems = [
      {
        label: 'Zur Wunschliste',
        routerLink: ['/']
      },
    ];

    this.tabs = [
      {
        label: 'Categories', icon: 'pi pi-fw pi-home', command: () => {
          this.activeTab = this.tabs[0]
        }
      },
      {
        label: 'Items', icon: 'pi pi-fw pi-calendar', command: () => {
          this.activeTab = this.tabs[1]
        }
      },
      {
        label: 'Users', icon: 'pi pi-fw pi-pencil', command: () => {
          this.activeTab = this.tabs[2]
        }
      },
    ];
    this.activeTab = this.tabs[0];

    this.getAllCategories()
    this.getAllItems()
  }

  async getAllCategories() {
    let {data, error} = await this.supabase.fetchCategories();

    if (error) {
      console.error(error);
    }

    this.categories = data ?? [];
  }

  async getAllItems(): Promise<void> {
    let {data, error} = await this.supabase.fetchItems();

    if (error) {
      console.error(error);
      return;
    }

    this.items = data ?? [];
  }

  logout() {
    this.supabase.signOut();
  }

  openNewCategoryDialog() {
    this.showNewCategoryDialog = true;
  }

  closeNewCategoryDialog() {
    this.showNewCategoryDialog = false;
    this.resetNewCategory();
  }

  saveNewCategoryDialog() {
    let data: AddCategory;

    console.log(this.newCategoryAction);

    if (this.newCategoryAction === this.DIALOG_ACTION_CREATE) {
      data = {
        name: this.newCategoryName,
        key: this.newCategoryKey
      }
    } else if (this.newCategoryAction === this.DIALOG_ACTION_UPDATE) {
      data = {
        id: this.newCategoryId,
        name: this.newCategoryName,
        key: this.newCategoryKey
      }
    } else {
      console.log("Upps");
      return;
    }

    console.log(data);

    this.supabase.writeCategory(data).then(() => {
      this.resetNewCategory();
      this.getAllCategories().then(() => this.closeNewCategoryDialog());
    });
  }

  resetNewCategory() {
    this.newCategoryAction = this.DIALOG_ACTION_CREATE;
    this.newCategoryId = 0;
    this.newCategoryName = "";
    this.newCategoryKey = "";
  }

  resetNewItem() {
    this.newItemAction = this.DIALOG_ACTION_CREATE;

    this.newItemId = 0;
    this.newItemName = "";
    this.newItemUrl = "";
    this.newItemImageurl = "";
    this.newItemPrice = 0;
    this.newItemCategory = 0;
    this.newItemDisabled = false;
  }

  updateCategoryDialog(category: Category) {
    this.newCategoryAction = this.DIALOG_ACTION_UPDATE;
    this.newCategoryId = category?.id;
    this.newCategoryName = category.name;
    this.newCategoryKey = category.key;

    this.openNewCategoryDialog();
  }

  deleteCategory(id: number) {
    this.supabase.deleteCategory(id).then(() => {
      this.getAllCategories();
    });
  }

  updateItemDialog(item: AddItem) {
    this.newItemId = item.id;
    this.newItemName = item.name;
    this.newItemUrl = item.url;
    this.newItemImageurl = item.imageurl;
    this.newItemPrice = item.price;
    this.newItemCategory = item.category;
    this.newItemDisabled = item.disabled;

    this.newItemAction = this.DIALOG_ACTION_UPDATE;

    this.openNewItemDialog();
  }

  deleteItem(id: number) {
    this.supabase.deleteItem(id).then(() => {
      this.getAllItems();
    })
  }

  openNewItemDialog() {
    this.showNewItemDialog = true;
  }

  closeNewItemDialog() {
    this.resetNewItem();
    this.showNewItemDialog = false;
  }

  saveNewItemDialog() {
    let data: AddItem;

    if (this.newItemAction === this.DIALOG_ACTION_CREATE) {
      data = {
        name: this.newItemName,
        url: this.newItemUrl,
        imageurl: this.newItemImageurl,
        price: this.newItemPrice,
        category: this.newItemCategory,
        disabled: false,
      }
    } else if (this.newItemAction === this.DIALOG_ACTION_UPDATE) {
      data = {
        id: this.newItemId,
        name: this.newItemName,
        url: this.newItemUrl,
        imageurl: this.newItemImageurl,
        price: this.newItemPrice,
        category: this.newItemCategory,
        disabled: this.newItemDisabled,
      }
    } else {
      console.log("Upps Item");
      return;
    }

    console.log(data);

    this.supabase.writeItem(data).then(() => {
      this.resetNewItem();
      this.getAllItems().then(() => this.closeNewItemDialog());
    });
  }

  getCategoryNameById(categoryId: number) {
    const r = this.categories.find((c) => {
      return (c.id === categoryId);
    })

    return r?.name;
  }

  updateNewItemCategoryId($event: Event) {
    console.log($event);
  }
}
